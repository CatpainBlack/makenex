#include "zx_basic.h"
#include <utility>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iostream>

using namespace std;

zx_basic::zx_basic(ZXBasicOptions options) : options(std::move(options)) {

}

int zx_basic::compile(const std::string &file) const {
	vector<string> c;
	c.push_back(options.zxbc);

	if (options.debug) {
		c.emplace_back("--debug");
	}
	if (options.explicit_) {
		c.emplace_back("--explicit");
	}
	if (options.zx_next) {
		c.emplace_back("--zxnext");
	}
	if (!options.arch.empty()) {
		c.emplace_back("--arch");
		c.push_back(options.arch);
	}

	c.push_back(file);

	std::ostringstream imploded;
	std::copy(c.begin(), c.end(), ostream_iterator<std::string>(imploded, " "));
	return system(imploded.str().c_str());
}


