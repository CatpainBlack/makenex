#pragma once

#include "nex_structs.h"

class Nex {

public:
	void setRequiredCoreVersion(uint8_t major, uint8_t minor, uint8_t revision);
	void setMemoryRequired(uint16_t ram);
	void setBorderColour(uint8_t colour);
	void enableLayer2LoadingBar(uint8_t colour);
	void setLoadingDelay(uint8_t frames);
	void setStartDelay(uint8_t frames);
	void setPC(uint16_t addr);
	void setSP(uint16_t addr);
	void setEntryBank(uint8_t bank);
	void setPreserveNextRegisters(bool preserve);

private:
	NexHeader header;

};


