#pragma once
#include <typeinfo>

typedef struct {
	uint8_t  signature[4]          = {'N', 'e', 'x', 't'};
	uint8_t  version[4]            = {'V', '1', '.', '2'};
	uint8_t  ram_required          = 0;
	uint8_t  bank_count            = 0;
	uint8_t  loading_screen_blocks = 0;
	uint8_t  border_colour         = 0;
	uint16_t stack_pointer         = 0;
	uint16_t program_counter       = 0;
	uint16_t no_extra_files        = 0;
	uint8_t  banks[112]{};
	uint8_t  l2_loading_bar        = 0;
	uint8_t  loading_bar_colour    = 0;
	uint8_t  loading_delay         = 0;
	uint8_t  start_delay           = 0;
	uint8_t  preserve_next_regs    = 0;
	uint8_t  core_required[3]      = {'2', '0', '0'};
	uint8_t  timex_hires_colour    = 0;
	uint8_t  entry_bank            = 0;
	uint16_t file_handle_addr      = 0;

	// V1.3
	uint8_t  enable_expansion_bus  = 0;
	uint8_t  has_checksum_value    = 0;
	uint32_t first_bank_offset     = 0;
	uint16_t cli_buffer_addr       = 0;
	uint16_t cli_buffer_size       = 0;
	uint8_t  loading_screen_flags2 = 0;
	uint8_t  has_copper_code_block = 0;
	uint8_t  tile_registers[4]{};
	uint8_t  loading_bar_y         = 0;
	uint8_t  _reserved[349]{};
	uint32_t crc32                 = 0;
} NexHeader;