#pragma once

#include <string>
#include <fstream>
#include <toml.hpp>

typedef struct {
	bool          debug{};
	unsigned char optimise_level{};
	bool          explicit_{};
	bool          zx_next{};
	std::string   arch;
	std::string   zxbc;
} ZXBasicOptions;

typedef struct {
	std::string name;
} NexOptions;

typedef struct {
	std::string   compiler;
	std::string   source;
	unsigned char bank{0};
	uint16_t      origin;
} Compile;

typedef struct {
	std::string          title;
	std::string          author;
	ZXBasicOptions       zx_basic;
	NexOptions           nex_opts;
	std::vector<Compile> builds;
} BuildOptions;

class TomlOptions {
public:
	static BuildOptions parse(const std::string &toml) {
		BuildOptions options;
		auto         config = toml::parse_file(toml);
		options.title  = config["title"].value_or("");
		options.author = config["author"].value_or("");

		auto zxb = config["zxbasic"];
		options.zx_basic.debug          = zxb["debug"].value_or(false);
		options.zx_basic.arch           = zxb["arch"].value_or("");
		options.zx_basic.explicit_      = zxb["explicit"].value_or(false);
		options.zx_basic.optimise_level = zxb["optimize"].value_or(2);
		options.zx_basic.zx_next        = zxb["zxnext"].value_or(false);
		options.zx_basic.zxbc           = zxb["zxbc"].value_or("");

		if (config["build"].is_array_of_tables()) {
			for (auto &&c:*config["build"].as_array()) {
				auto    t = c.as_table();
				Compile compile;
				if (t->contains("compiler")) {
					compile.compiler = t->get("compiler")->value_or("zxbc");
				}

				if (t->contains("source")) {
					compile.source = t->get("source")->value_or("");
				} else {
					throw toml::parse_error("Source is required", t->source());
				}

				if (t->contains("origin")) {
					compile.origin = t->get("origin")->value_or(32768);
				} else {
					compile.origin = 32768;
				}

				if (t->contains("bank")) {
					compile.bank = t->get("bank")->value_or(0);
				} else {
					compile.bank = 5;
				};

				options.builds.push_back(compile);
			}
		}

		return options;
	}
};