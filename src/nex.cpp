#include "nex.h"

void Nex::setRequiredCoreVersion(uint8_t major, uint8_t minor, uint8_t revision) {
	this->header.core_required[0] = major;
	this->header.core_required[1] = minor;
	this->header.core_required[2] = revision;
}

void Nex::setMemoryRequired(uint16_t ram) {
	switch (ram) {
		case 1792:
			this->header.ram_required = 1;
			break;
		case 768:
		default:
			this->header.ram_required = 0;
			break;
	}
}

void Nex::setBorderColour(uint8_t colour) {
	this->header.border_colour = colour;
}

void Nex::enableLayer2LoadingBar(uint8_t colour) {
	this->header.l2_loading_bar     = 1;
	this->header.loading_bar_colour = colour;
}

void Nex::setLoadingDelay(uint8_t frames) {
	this->header.loading_delay = frames;
}

void Nex::setStartDelay(uint8_t frames) {
	this->header.start_delay = frames;
}

void Nex::setPC(uint16_t addr) {
	this->header.program_counter = addr;
}

void Nex::setSP(uint16_t addr) {
	this->header.stack_pointer = addr;
}

void Nex::setEntryBank(uint8_t bank) {
	this->header.entry_bank = bank;
}

void Nex::setPreserveNextRegisters(bool preserve) {
	this->header.preserve_next_regs = preserve;
}
