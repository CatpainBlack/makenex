#pragma once
#include <string>
#include "build_opts.h"

class zx_basic {
public:
	zx_basic(ZXBasicOptions options);

	int compile(const std::string &file) const;

private:
	ZXBasicOptions options;
};

