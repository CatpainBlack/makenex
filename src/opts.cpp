#include <argh.h>
#include "opts.h"

opts::opts() : verbose(false), input("build.toml") {

}

void opts::parse(char **argv) {
	argh::parser command_line(argv);
	verbose = command_line[{"-v", "--verbose"}];
	if (!command_line[1].empty()) {
		input = command_line[1];
	}
}
