#include <iostream>
#include <toml.hpp>
#include <fmt/core.h>

#include "opts.h"
#include "build_opts.h"
#include "nex_structs.h"
#include "zx_basic.h"

using namespace std;

int main(int, char *argv[]) {
	opts o;
	o.parse(argv);

	NexHeader nh;

//	fmt::print("Header: {}\n", sizeof nh);

	try {
		auto buildOptions = TomlOptions::parse(o.input);
		fmt::print("Building {}\n", buildOptions.title);

		zx_basic z(buildOptions.zx_basic);

		for (const auto &value:buildOptions.builds) {
			fmt::print("Build: {} (Bank: {})\n", value.source, value.bank);
			z.compile(value.source);
		}
	}

	catch (const toml::parse_error &err) {
		cerr << err.description() << endl
			 << err.source() << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
